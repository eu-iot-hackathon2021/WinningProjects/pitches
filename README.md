# Pitches

Pitching slides presented on 28.06.2022 on the EU-IoT/EFPF Hackathon

_**First Place: Team 6, Cassio Dias, Felipe da Silva Braz, Gabriel Negri, Jose Angelo de Oliveira, UNIVESP, Brazil**_
**Awards**:
- EU-IoT Challenges Award, provided by UnternehmerTUM Makerspace – 1 year XL membership for incubation in the UnternhemerTUM Makerspace in Munich and 3 trainings per team member.
- EFPF Challenges Award first prize: 1 Google Pixel 6 Smartphone

_**Second Place: Team 2, Sudhir Kshirsagar (PhD), Global Quality Corp., Palo Alto, California**_
**Awards**:
- IoT Forum voucher for IoT Week2023, 1 person
- EFPF Challenges Award, 2nd prize - Google Next HUB 2nd generation smart display and Google ChromeCast Stream player, and 2 Google Nest Audio Smart Speakers

_**Third place: Team 16, Bruno Lowcsy, UNIVESP, Brazil**_
**Awards:**
EFPF Challenges Award 3rd prize, Arduino Kit Explore Education and a set of 40 sensors for Arduino projects, plus Arduino Sensor Kit Base with Shield and 10 Grove Sensors.

**In addition, all team members (individual persons) have been awarded with:**

- Infineon Kit I(PSoC™ 62S2 Wi-Fi BT Pioneer Kit (CY8CKIT-062S2-43012) and IoT Sense Expansion Kit. CY8CKIT-062S2-43012 and 10x CY8CKIT-028-SENSE. The PSoC™ 62S2 Wi-Fi BT Pioneer Kit (CY8CKIT-062S2-43012) is a low-cost hardware platform that enables design and debug of the PSoC™ 62 MCU and the Murata 1LV Module (CYW43012 Wi-Fi + Bluetooth Combo Chip). The IoT sense expansion kit (Y8CKIT-028-SENSE) is a low-cost Arduino™ UNO compatible shield board that can be used to easily interface a variety of sensors with the PSoC™ 6 MCU platform, specifically targeted for audio and machine learning applications.



- Infineon Kit II CY8CPROTO-062-4343W. PSoC™ 6 Wi-Fi BT Prototyping Kit (CY8CPROTO-062-4343W) is a low-cost hardware platform that enables design and debug of PSoC™ 6 MCUs. It comes with a CY8CMOD-062-4343W module, industry-leading CAPSENSE™ for touch buttons and slider, on-board debugger/programmer with KitProg3, microSD card interface, 512-Mb Quad-SPI NOR flash, PDM microphone, and a thermistor. It also includes a Murata LBEE5KL1DX module, based on the CYW4343W combo device.















